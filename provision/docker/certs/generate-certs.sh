#!/bin/bash -e

# registry.ws.so
openssl req -newkey rsa:4096 -days 365 -nodes -sha256 \
    -keyout registry.key -x509 \
    -out registry.crt \
    -subj "/C=IT/ST=Milan/L=Milan/O=Workshop/OU=Local/CN=registry.ws.so"
