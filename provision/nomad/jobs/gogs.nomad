job "gogs" {
  datacenters = ["dc1"]
  type        = "service"

  update {
    stagger      = "10s"
    max_parallel = 1
  }

  group "gogs" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "fail"
    }

    ephemeral_disk {
      size = 300
    }

    task "gogs" {
      driver = "docker"

      config {
        image = "gogs/gogs:0.11.91"

        port_map {
          ssh  = 3022
          http = 3080
        }

        volumes = [
          "/vagrant/data/gogs:/data",
        ]
      }

      env {
        RUN_CROND = 1
      }

      resources {
        cpu    = 300
        memory = 256

        network {
          port "ssh"{}
          port "http"{}
        }
      }

      service {
        name = "gogs"
        tags = ["global", "gogs", "http", "urlprefix-git.ws.so/"]
        port = "http"

        check {
          type = "tcp"

          # type         = "http"
          # path         = "/healthcheck"
          interval = "10s"

          timeout = "2s"
        }
      }

      service {
        name = "gogs-ssh"
        tags = ["global", "git", "ssh", "urlprefix-:${NOMAD_PORT_ssh} proto=tcp"]
        port = "ssh"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
