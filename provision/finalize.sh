#!/bin/bash -e

PAAS_IP=${PAAS_IP:-${1:-"192.168.56.10"}}

apt-get -y autoremove
apt-get -y autoclean
apt-get -y clean

echo -e "\n== Provisioning done.\n"

echo -e "\n== Add the following lines to your host machine /etc/hosts\n"
echo "#### BOX PaaS"
grep ${PAAS_IP} /etc/hosts
echo "####"

echo -e "\n== Then you can access some service here:\n\n"
cat <<EOF

** Nomad        > http://nomad.ws.so/
** Consul       > http://consul.ws.so/
** Fabio        > http://fabio.ws.so/
** Registry     > http://registry.ws.so/v2/
** Prometheus   > http://prometheus.ws.so/
** Grafana      > http://grafana.ws.so/ [user: admin, pass: admin]
** Gogs         > http://git.ws.so/ [user: gogs, pass: gogs]
** Jenkins      > http://jenkins.ws.so/ [user: jenkins, pass: jenkins]

EOF

echo -e "\n== All services will be ready in a while, be patient ;)\n\n"
