#!/bin/bash -e

echo "Installing Jenkins..."
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://pkg.jenkins.io/debian binary/"
apt-get update -y -q

apt-get install -y -q -f openjdk-11-jre-headless

apt-get purge -y jenkins || true
cp /vagrant/provision/jenkins/etc/jenkins /etc/init.d/jenkins
apt-get install -y -q -f -o Dpkg::Options::="--force-confold" jenkins=2.254 || true

systemctl stop jenkins.service

usermod -aG ubuntu jenkins
usermod -aG docker jenkins
usermod -aG jenkins vagrant

echo 'JENKINS_ARGS="$JENKINS_ARGS --argumentsRealm.passwd.jenkins=jenkins --argumentsRealm.roles.jenkins=admin"' >>/etc/default/jenkins

cp -av /vagrant/data/jenkins/* /var/lib/jenkins/
chown -R jenkins:jenkins /var/lib/jenkins/

# chown -R ubuntu:ubuntu /var/lib/jenkins/
# cp -va /vagrant/provision/nomad/systemd/nomad-job.service /etc/systemd/system/jenkins.service
# cp -av /vagrant/provision/nomad/jobs/jenkins.nomad /etc/nomad.jobs/
systemctl enable jenkins.service
systemctl restart jenkins.service

# Build generic docker-in-docker slave image
#(
#    cd /vagrant/provision/jenkins/images/jenkins-slave-dind
#    docker build --rm -t registry.ws.so/jenkins-slave-dind .
#    docker push registry.ws.so/jenkins-slave-dind
#)

# Build docker-in-docker slave image with PHP
#(
#    cd /vagrant/provision/jenkins/images/jenkins-slave-dind-php
#    docker build --rm -t registry.ws.so/jenkins-slave-dind-php .
#    docker push registry.ws.so/jenkins-slave-dind-php
#)
