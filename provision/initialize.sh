#!/bin/bash -e

export PAAS_IP=${PAAS_IP:-${1:-"192.168.56.10"}}
export DATA_DIR="/vagrant/data"

export LANG="en_US.UTF-8"
export LANGUAGE="en_US:en"
export LC_ALL="en_US.UTF-8"
export TIMEZONE="Europe/Rome"

echo LC_ALL="${LC_ALL}" >> /etc/environment
echo LANGUAGE="${LANGUAGE}" >> /etc/environment

locale-gen ${LANG}
timedatectl --adjust-system-clock set-timezone ${TIMEZONE} || echo ${TIMEZONE} > /etc/timezone

dpkg-reconfigure -f noninteractive tzdata

# tools
apt-get update -y -q && apt-get install -y -q \
	ubuntu-keyring language-pack-en language-pack-it \
	wget curl git unzip vim netcat socat tcpdump dnsutils authbind

# user profile
usermod -aG ubuntu vagrant
adduser vagrant vboxsf || true
(cat <<-EOF
# box-paas custom
export PAAS_IP=${PAAS_IP}
export DATA_DIR=${DATA_DIR}
EOF
) | sudo -u vagrant tee -a ~vagrant/.profile

(cat <<-EOF
[client]
protocol=tcp
EOF
) | sudo -u vagrant tee ~vagrant/.my.cnf

# copy provided id_rsa key pair
mkdir ~vagrant/.ssh || true
chown vagrant:vagrant ~vagrant/.ssh
chmod 700 ~vagrant/.ssh
cp -va /vagrant/provision/id_rsa.git* ~vagrant/.ssh/
(cat <<-EOF
Host *
  ControlMaster auto
  ControlPath /tmp/ssh-%r@%h:%p
  ControlPersist 600

Host git.ws.so
  User git
  Port 3022
  IdentityFile ~/.ssh/id_rsa.git
EOF
) | sudo -u vagrant tee ~vagrant/.ssh/config

# initialize data directories
mkdir -p ${DATA_DIR} || true
chown vagrant:vagrant ${DATA_DIR} || true
chmod 775 ${DATA_DIR} || true
(cd ${DATA_DIR} 
mkdir -p gogs jenkins repository || true
chmod 775 ${DATA_DIR} gogs jenkins repository || true
)

# combine /etc/hosts
[ ! -f /etc/hosts.orig ] && cp -v /etc/hosts /etc/hosts.orig
cat /etc/hosts.orig /vagrant/provision/hosts >/etc/hosts
