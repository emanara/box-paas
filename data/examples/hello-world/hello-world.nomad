job "hello-world" {
  datacenters = ["dc1"]
  type = "service"

  update {
    max_parallel = 3
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 3
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "cache" {
    count = 3

    restart {
      attempts = 10
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 50
    }

    task "hello-world" {
      driver = "docker"

      config {
        image = "registry.ws.so/hello-world"

        port_map {
          http = 8080
        }
      }

     resources {
        cpu    = 100 # 500 MHz
        memory = 32 # 256MB

        network {
          mbits = 3
          port "http" {}
        }
      }

     service {
        name = "hello-world"
        tags = ["global", "go", "hello-world", "urlprefix-hello-world.ws.so/", "urlprefix-hello-ws.so/hello-world"]
        port = "http"

        check {
          path     = "/ping"
          type     = "http"
          interval = "10s"
          timeout  = "2s"
        }
      }
   }
  }
}
