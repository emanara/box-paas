#!/bin/bash -e

docker build --rm -t registry.ws.so/hello-world .

if [[ "$1"x = "-push"x ]] ; then
    docker push registry.ws.so/hello-world
fi